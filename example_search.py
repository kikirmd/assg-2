from tester import test_config_equality
from support.robot_config import write_robot_config_list_to_file
from support.problem_spec import ProblemSpec

"""
Example of code for performing BFS on a state graph, including a class representing a state graph node.

COMP3702 2019 Assignment 2 Support Code

Last updated by njc 17/09/19
"""


class GraphNode:
    """
    Class representing a node in the state graph. You should create an instance of this class each time you generate
    a sample.
    """

    def __init__(self, spec, config):
        """
        Create a new graph node object for the given config.

        Neighbors should be added by appending to self.neighbors after creating each new GraphNode.

        :param spec: ProblemSpec object
        :param config: the RobotConfig object to be stored in this node
        """
        self.spec = spec
        self.config = config
        self.neighbors = []

    def __eq__(self, other):
        return test_config_equality(self.config, other.config, self.spec)

    def __hash__(self):
        return hash(tuple(self.config.points))

    def get_successors(self):
        return self.neighbors



def solve(spec):
    """
    An example solve method containing code to perform a breadth first search of the state graph and return a list of
    configs which form a path through the state graph between the initial and the goal. Note that this path will not
    satisfy the primitive step requirement - you will need to interpolate between the configs in the returned list.

    If you wish to use this code, you may either copy this code into your own file or add your existing code to this
    file.

    :param spec: ProblemSpec object
    :return: List of configs forming a path through the graph from initial to goal
    """

    init_node = GraphNode(spec, spec.initial)
    goal_node = GraphNode(spec, spec.goal)

    
    # di function solve(spec):
    #   1. kalo mau interleave, while runtime < timelimit AND path is not found:
    #       1. init_container = call create_grah(spec) or something
    #       2. search init_container for a path

    # search the graph
    init_container = [init_node]

    # here, each key is a graph node, each value is the list of configs visited on the path to the graph node
    init_visited = {init_node: [init_node.config]}

    while len(init_container) > 0:
        current = init_container.pop(0)

        if test_config_equality(current.config, spec.goal, spec):
            # found path to goal
            return init_visited[current]

        successors = current.get_successors()
        for suc in successors:
            if suc not in init_visited:
                init_container.append(suc)
                init_visited[suc] = init_visited[current] + [suc.config]

def main(arglist):
    spec = ProblemSpec(arglist[0])
    list_of_configs = solve(spec)
    write_robot_config_list_to_file(arglist[1], list_of_configs)

    # ProblemSpec = grapple_points, obstacles, initial, goal, num_grapple_points, num_segments, num_obstacles
    # RobotConfig = cek sendiri
    #
    # make_robot_config_from_ee1(x, y, angles, lengths, ee1_grappled=False, ee2_grappled=False):
    # RobotConfig(lengths, ee1x=x, ee1y=y, ee1_angles=angles, ee1_grappled=ee1_grappled, ee2_grappled=ee2_grappled
    # jadi kalo make from ee1, yg di set ee1x sama ee1y
    # ee1 is grappled, use make from ee1
    # ee2 is grappled, use make from ee2
    # to interpolate between points grappled to ee1, use make from ee1 and vice versa
    # to interpolate between bridges, use either. bridges = grappled at both ends
    # question: HOW do you make bridges? bisa lol tinggal use either terus bikin true for both grappled
    #
    # create graph di class Graph (gatau perlu bikin class apa engga, mungkin bikin file yg isinya functions aja):
    #   1. config = sample()
    #   2. if not collision(config): create GraphNode, add to list of nodes, connect(config)
    #   3. repeat until say 500 nodes or something
    #
    #
    # list of functions in graph.py:
    #   1. sample() # bisa beda2 lho methodnya, bisa random, near obstacle, etc
    #   2. collision(config)
    #   3. connect(config) # connect config to existing nodes in a certain radius from node (distance between joints atau distance ujungnya doang idk)
    #   4. dist_nearest_obstacle(config) # find distance OF FIRST JOINT to nearest obstacle, for use in collision check
    #
    # isi function connect(q1):
    #   1. potential_neighbors = get all (or like 10 or something) nodes in list that is sekian distance dari config
    #   2. for each q2 in potential_neighbors:
    #       collision = false
    #       for each joint of q1:
    #           if !distance_computation(j1 dr q1, j1 dr q2):
    #               collision = true
    #               BREAK
    #       if !collision:
    #           q1.neighbors.append(q2)
    #           q2.neighbors.append(q1)
    #
    # distance_computation(config, node):
    # if collision(config) or collision(node): return false
    # d1 = dist_nearest_obstacle(config)
    # d2 = dist_nearest_obstacle(node)
    # midpoint = (x1+x2/2,  etc)
    # if distance midpoint to d1 < d1 and distance midpoint to d2 < d2: return true # no collision
    # else: return distance_computation(config, midpoint) and distance_computation(node, midpoint)
    #
    #
    # interpolate(c1, c2):
    #




